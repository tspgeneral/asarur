# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version 0.0.1
* [Learn Java Spring](https://spring.io/)
* Open source GNU GPL
* Source delivered to the UTN-Frre-TSP-DB course
* Website ASARUR Resistencia Chaco
* ASARUR It is an institution of regional art

### How do I get set up? ###

* Summary of set up

 The language used in Java, called the Spring MVC framework, using Maven, for the deployment of the application. The STS Eclipse IDE is used and the database was done with PostgreSQL, using the administrator pgadmin3

* Configuration
 
 You Need Maven pom.xml, the Asarudb.backup, Apache Tomcat 7,8,9. 

* Dependencies
 
 Dependencies are loaded on file with maven

* Database configuration

 The database engine takes installed [PostgreSQL](http://www.postgresql.org/) 

* Deployment instructions

 Compiling with MAVEN and then use Tomcat as a server.

### Contribution guidelines ###

* Code review
 German Gaona 
* Other guidelines
 David Portela
 Facundo Valissi
 Alberto Robledo

### Who do I talk to? ###

* Admin Repo: User beto1234
* Other community or team contact
 Email: roblo53@hotmail.com
 Alberto Robledo