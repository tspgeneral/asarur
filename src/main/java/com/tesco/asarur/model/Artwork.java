package com.tesco.asarur.model;
import java.util.ArrayList;
import java.util.List;
import org.springframework.web.multipart.MultipartFile;

public class Artwork implements java.io.Serializable{
	/**
	 * 
	 */
	private String title;
	private String esPrivate;
	private String state;
	private String observation;
	private String dateImp;
	private String address;
	private String number;
	private String dniPersonRev;
	private String dateRev;
	private String dniAuthor;
	private String creationDate;
	private List<String> nomMaterial = new ArrayList<String>();
	private String nomCategory;
	private List<MultipartFile> files;
	private String coordinate;
	private int numVote;
	
	
	public int getNumVote() {
		return numVote;
	}

	public void setNumVote(int numVote) {
		this.numVote = numVote;
	}

	public String getTitle() {
		return title;
	}

	public String getCoordinate() {
		return coordinate;
	}

	public void setCoordinate(String coordinate) {
		this.coordinate = coordinate;
	}

	public List<MultipartFile> getFiles() {
		return files;
	}

	public void setFiles(List<MultipartFile> files) {
		this.files = files;
	}

	public List<String> getNomMaterial() {
		return nomMaterial;
	}

	public void setNomMaterial(List<String> nomMaterial) {
		this.nomMaterial = nomMaterial;
	}

	public String getDniPersonRev() {
		return dniPersonRev;
	}

	public void setDniPersonRev(String dniPersonRev) {
		this.dniPersonRev = dniPersonRev;
	}

	public String getDateRev() {
		return dateRev;
	}

	public void setDateRev(String dateRev) {
		this.dateRev = dateRev;
	}

	public String getDniAuthor() {
		return dniAuthor;
	}

	public void setDniAuthor(String dniAuthor) {
		this.dniAuthor = dniAuthor;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}
	public String getNomCategory() {
		return nomCategory;
	}

	public void setNomCategory(String nomCategory) {
		this.nomCategory = nomCategory;
	}

	public String getEsPrivate() {
		return esPrivate;
	}

	public void setEsPrivate(String esPrivate) {
		this.esPrivate = esPrivate;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getObservation() {
		return observation;
	}

	public void setObservation(String observation) {
		this.observation = observation;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getDateImp() {
		return dateImp;
	}

	public void setDateImp(String dateImp) {
		this.dateImp = dateImp;
	}

	
	
}
