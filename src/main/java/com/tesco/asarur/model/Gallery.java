package com.tesco.asarur.model;

public class Gallery implements java.io.Serializable{
	private String title;
	private String address;
	private int number;
	private String urlImage;
	private int cod;
	
	public Gallery(String title, String address, int number, String urlImage,int cod) {
		super();
		this.title = title;
		this.address = address;
		this.number = number;
		this.urlImage = urlImage;
		this.cod= cod;
	}
	public int getCod() {
		return cod;
	}
	public void setCod(int cod) {
		this.cod = cod;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	public String getUrlImage() {
		return urlImage;
	}
	public void setUrlImage(String urlImage) {
		this.urlImage = urlImage;
	}
}
