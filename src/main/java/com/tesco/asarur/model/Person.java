package com.tesco.asarur.model;

public class Person implements java.io.Serializable{
	long documentoNumero;
	String isEstudiante;
	String isArtista;
	String telefono;
	String apellido;
	String nombre;
	String documentoTipo;
	String direccionCalle;
	int direccionNumero;
	public long getDocumentoNumero() {
		return documentoNumero;
	}
	public void setDocumentoNumero(long documentoNumero) {
		this.documentoNumero = documentoNumero;
	}
	public String getIsEstudiante() {
		return isEstudiante;
	}
	public void setIsEstudiante(String isEstudiante) {
		this.isEstudiante = isEstudiante;
	}
	public String getIsArtista() {
		return isArtista;
	}
	public void setIsArtista(String isArtista) {
		this.isArtista = isArtista;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDocumentoTipo() {
		return documentoTipo;
	}
	public void setDocumentoTipo(String documentoTipo) {
		this.documentoTipo = documentoTipo;
	}
	public String getDireccionCalle() {
		return direccionCalle;
	}
	public void setDireccionCalle(String direccionCalle) {
		this.direccionCalle = direccionCalle;
	}
	public int getDireccionNumero() {
		return direccionNumero;
	}
	public void setDireccionNumero(int direccionNumero) {
		this.direccionNumero = direccionNumero;
	}
	
}
