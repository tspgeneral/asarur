package com.tesco.asarur.controller;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FilenameUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.tesco.asarur.model.Gallery;


/**
 * Handles requests for the application home page.
 */
@Controller
public class CatalogController {
	@RequestMapping(value = "/Catalog", method = RequestMethod.GET)
	public ModelAndView selectTag() {
		ModelAndView mav = new ModelAndView("Catalog");// siempre hay que
														// devolver el modelo
														// con el nombre de la
														// vista
		return mav;
	}
	@RequestMapping(value="/Catalog/voto/{cod}/{dni}", method= RequestMethod.GET)
	public String deleteContact(@PathVariable("cod") int cod,@PathVariable("dni") int dni){
		//service.delete(id);
		System.out.println("----Voto------");
		System.out.println(cod + " " + dni);
		java.util.Date utilDate = new java.util.Date(); //fecha actual
		Connection c = null;
		PreparedStatement stmt = null;
		Statement stmtO = null;
		try {
			Class.forName("org.postgresql.Driver");
			c = DriverManager.getConnection("jdbc:postgresql://localhost:5432/asarurdb", "tesco", "beto1234");
			c.setAutoCommit(false);
			System.out.println("Opened database successfully");
			String sqlV = "INSERT INTO \"ObraPerson\"( \"obraCodObra\", \"personaDocumentoNumero\", \"fechaVoto\") VALUES (?, ?, ?)";
			stmt = c.prepareStatement(sqlV);
			stmt.setInt(1, cod);
			stmt.setInt(2, dni);
			stmt.setDate(3, new java.sql.Date(utilDate.getTime()));
			stmt.execute();
			c.commit();
			//Buscamos La cantidad de voto
			stmtO = c.createStatement();
			String sqlNumV="SELECT \"canVoto\" FROM \"Obra\" WHERE \"codObra\"=" + cod;
			ResultSet rsA = stmtO.executeQuery(sqlNumV);
			int numV=0;
			while (rsA.next()) {
				numV=rsA.getInt("canVoto");
			}
			rsA.close();
			numV+=1;
			System.out.println("NumVoto" + numV);
			String sqlAct ="UPDATE \"Obra\" SET \"canVoto\"="+ numV +" WHERE \"codObra\"=" + cod;
			stmtO.executeUpdate(sqlAct);
	        c.commit();
			//cierra todo
	        stmtO.close();
			stmt.close();
			c.close();//cierra la conexion a la bd
		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			//System.exit(0);
			return "redirect:/Persona";
		}
		System.out.println("Records created successfully");
		return "redirect:/Catalog";
	}
	@RequestMapping(value = "/Catalog", method = RequestMethod.POST)
	public String addCatalog(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			Model model) {
		Connection c = null;
		Statement stmt = null;
		List<Gallery> listGal = new ArrayList<Gallery>();
		System.out.println("===Ingresa a catalog post===");
		try {
			Class.forName("org.postgresql.Driver");
			c = DriverManager.getConnection("jdbc:postgresql://localhost:5432/asarurdb", "tesco", "beto1234");
			c.setAutoCommit(false);
			System.out.println("Opened database successfully");
			stmt = c.createStatement();
			String sqlC="SELECT DISTINCT ON(O.\"codObra\") \"codObra\", titulo, "
					+ "\"fechaImplementacion\",\"direccionCalle\",\"direccionNumero\", imagen, nombre"
					+ " FROM \"Obra\" O"
					+ " INNER JOIN \"GaleriaImagen\" GI"
					+ " ON O.\"codObra\"=GI.\"obraCodObra\"";
			ResultSet rs2 = stmt.executeQuery(sqlC);
			String namearchi;
			while (rs2.next()) {
				System.out.println("crea el archivo");
				namearchi = rs2.getString("nombre");
				if (!namearchi.equalsIgnoreCase("")) {
					// convert byte array back to BufferedImage
					InputStream in = new ByteArrayInputStream(rs2.getBytes("imagen"));
					BufferedImage bImageFromConvert = ImageIO.read(in);
					String ext = FilenameUtils.getExtension(namearchi);
					String absoluteDiskPath = request.getSession().getServletContext().getRealPath("/resources/img/")
							+ namearchi;
					System.out.println(absoluteDiskPath);
					ImageIO.write(bImageFromConvert, ext, new File(absoluteDiskPath));
					in.close();
					String url = "/asarur/resources/img/" + namearchi;
					listGal.add(new Gallery(rs2.getString("titulo"),rs2.getString("direccionCalle"),rs2.getInt("direccionNumero"),url,rs2.getInt("codObra")));
				}
			}

			rs2.close();
			stmt.close();
			c.close();
			session.setAttribute("listGal",listGal);
		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			return "redirect:/Error";
			//System.exit(0);
		}
		System.out.println("Operation done successfully");
		// ********End conect DB***********************************
		return "redirect:/Catalog";
	}

}
