package com.tesco.asarur.controller;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.tesco.asarur.model.Artwork;

@Controller
public class ArtworkController {
	
	@RequestMapping(value = "/Artwork", method = RequestMethod.GET)
	public ModelAndView selectTag() {
		ModelAndView mav = new ModelAndView("Artwork");// siempre hay que
														// devolver el modelo
														// con el nombre de la
														// vista
		Map<Integer, String> categoria = new HashMap<Integer, String>();
		Map<Integer, String> material = new HashMap<Integer, String>();
		Map<Integer, String> autor = new HashMap<Integer, String>();
		// ***Conexion a la Base de datos para obtener la lista de materiales.
		Connection c = null;
		Statement stmt = null;
		try {
			Class.forName("org.postgresql.Driver");
			c = DriverManager.getConnection("jdbc:postgresql://localhost:5432/asarurdb", "tesco", "beto1234");
			c.setAutoCommit(false);
			System.out.println("Opened database successfully");
			stmt = c.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT * FROM \"Material\"");
			while (rs.next()) {
				material.put(rs.getInt("codMaterial"), rs.getString("nombre"));
			}
			rs.close();
			ResultSet rs2 = stmt.executeQuery("SELECT * FROM \"Categoria\"");
			while (rs2.next()) {
				categoria.put(rs2.getInt("codCategoria"), rs2.getString("nombre"));
			}
			rs2.close();
			ResultSet rs3 = stmt.executeQuery("SELECT * FROM \"Autor\"");
			while (rs3.next()) {
				autor.put(rs3.getInt("codAutor"), rs3.getString("nombre") + " " + rs3.getString("apellido"));
			}
			rs3.close();
			stmt.close();
			c.close();
		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			// System.exit(0);
		}
		System.out.println("Operation done successfully");
		// ********End conect DB***********************************
		mav.addObject("autorMap", autor);
		mav.addObject("categoriaMap", categoria);
		mav.addObject("materialMap", material);
		mav.addObject("command", new Artwork());

		return mav;
	}

	@RequestMapping(value = "/Artwork", method = RequestMethod.POST)
	public String addArtwork(@ModelAttribute("artwork") Artwork artwork, BindingResult result, Model map) {
		// ***DB insert Artwork*************************************
		Connection c = null;
		PreparedStatement stmt = null;
		Statement stmtArt = null;
		try {
			Class.forName("org.postgresql.Driver");
			c = DriverManager.getConnection("jdbc:postgresql://localhost:5432/asarurdb", "tesco", "beto1234");
			c.setAutoCommit(false);
			System.out.println("Opened database successfully");
			String sql = "INSERT INTO \"Obra\" (" + "\"canVoto\", estado, observacion, \"isPrivada\", titulo,"
					+ "\"fechaImplementacion\", \"imagenEmplazamiento\", \"direccionCalle\","
					+ "\"direccionNumero\", \"fechaRevela\", \"fechaCreacion\", \"autorCodAutor\","
					+ "\"categoriaCodCategoria\", \"dniRevela\") "
					+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
			stmt = c.prepareStatement(sql);
			stmt.setInt(1, 0);// canVoto = 0
			stmt.setString(2, artwork.getState());// estado
			stmt.setString(3, artwork.getObservation());// observacion
			stmt.setBoolean(4, Boolean.parseBoolean(artwork.getEsPrivate()));
			stmt.setString(5, artwork.getTitle());// titulo
			SimpleDateFormat fr = new SimpleDateFormat("dd/MM/yyyy");
			Date parsedImp = fr.parse(artwork.getDateImp());
			stmt.setDate(6, new java.sql.Date(parsedImp.getTime()));// fechaImplementacion
			stmt.setString(7, artwork.getCoordinate());// coordenada imagen
														// Emplaza
			stmt.setString(8, artwork.getAddress());// direccionCalle
			stmt.setInt(9, Integer.parseInt(artwork.getNumber()));// direccionNumero
			Date parsedRev = fr.parse(artwork.getDateRev());
			stmt.setDate(10, new java.sql.Date(parsedRev.getTime()));// fechaRevela
			Date parsedCre = fr.parse(artwork.getCreationDate());
			stmt.setDate(11, new java.sql.Date(parsedCre.getTime()));// fechaCreacion
			stmt.setInt(12, Integer.parseInt(artwork.getDniAuthor()));// autorCodAutor
			stmt.setInt(13, Integer.parseInt(artwork.getNomCategory()));// categoriaCodCategoria
			stmt.setInt(14, Integer.parseInt(artwork.getDniPersonRev()));// dniRevela
			stmt.execute();
			c.commit();
			// Buscamos la ultima obra subida su cod
			stmtArt = c.createStatement();
			ResultSet rsA = stmtArt.executeQuery("SELECT MAX(\"codObra\") AS codObra FROM \"Obra\"");
			int codObra = 0;
			while (rsA.next()) {
				codObra = rsA.getInt("codObra");
			}
			System.out.println("CodObra: " + codObra);
			rsA.close();
			stmtArt.close();
			// insertamos la lista de materiales de esa obra
			String sql2 = "INSERT INTO \"ObraMaterial\"(\"materialCodMaterial\", \"obraCodObra\") VALUES (?, ?)";
			for (String lisCodMat : artwork.getNomMaterial()) {
				System.out.println("CodMaterial: " + lisCodMat);
				stmt = c.prepareStatement(sql2);
				stmt.setInt(1, Integer.parseInt(lisCodMat));// materialCodMaterial
				stmt.setInt(2, codObra);// obraCodObra
				stmt.execute();
				c.commit();
			}
			System.out.println("Cargamos las imagenes a la galeria");
			// insertamos las imagenes en la galeria
			String sql3 = "INSERT INTO \"GaleriaImagen\"(imagen, \"obraCodObra\", nombre) VALUES (?, ?, ?)";
			for (MultipartFile imageGal : artwork.getFiles()) {
				System.out.println(imageGal.getOriginalFilename());
				stmt = c.prepareStatement(sql3);
				stmt.setBinaryStream(1, imageGal.getInputStream());// imagen
				stmt.setInt(2, codObra);// obraCodObra
				stmt.setString(3, imageGal.getOriginalFilename());
				stmt.execute();
				c.commit();
			}
			// cierra todo
			stmt.close();
			c.close();// cierra la conexion a la bd
		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			return "redirect:/Error";
		}
		System.out.println("Records created successfully");
		// ***End DB insert Artwork**********************************
		return "redirect:/Artwork";
	}

}
