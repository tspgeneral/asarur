package com.tesco.asarur.controller;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.tesco.asarur.model.Artwork;
@Controller
public class RankingController {
		@RequestMapping(value = "/Ranking", method = RequestMethod.GET)
		public ModelAndView selectTag() {
			ModelAndView mav = new ModelAndView("Ranking");
			List<Artwork> topArt = new ArrayList<Artwork>();
			Connection c = null;
			Statement stmt = null;
			try {
				Class.forName("org.postgresql.Driver");
				c = DriverManager.getConnection("jdbc:postgresql://localhost:5432/asarurdb", "tesco", "beto1234");
				c.setAutoCommit(false);
				System.out.println("Opened database successfully");
				stmt = c.createStatement();
				String sqlC="SELECT * FROM \"Obra\" ORDER BY \"canVoto\" DESC LIMIT 10";
				ResultSet rs2 = stmt.executeQuery(sqlC);
				while (rs2.next()) {
					Artwork artw = new Artwork();
					artw.setNumVote(rs2.getInt("canVoto"));
					artw.setTitle(rs2.getString("titulo"));
					artw.setAddress(rs2.getString("direccionCalle"));
					artw.setNumber(String.valueOf(rs2.getInt("direccionNumero")));
					artw.setCreationDate(rs2.getDate("fechaImplementacion").toString());
					artw.setState(rs2.getString("estado"));
					artw.setObservation(rs2.getString("observacion"));
					topArt.add(artw);
				}
				rs2.close();
				stmt.close();
				c.close();
				mav.addObject("listTop", topArt);
				//session.setAttribute("listGal",listGal);
			} catch (Exception e) {
				System.err.println(e.getClass().getName() + ": " + e.getMessage());
				//System.exit(0);
				
			}
			System.out.println("Operation done successfully");
			// ********End conect DB***********************************		
			
			return mav;
		}

}
