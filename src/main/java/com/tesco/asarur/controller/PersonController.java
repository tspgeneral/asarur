package com.tesco.asarur.controller;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.Statement;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.tesco.asarur.model.Artwork;
import com.tesco.asarur.model.Person;


@Controller
public class PersonController {
	@RequestMapping(value = "/Persona", method = RequestMethod.GET)
	private ModelAndView selectTag() {
		ModelAndView mav = new ModelAndView("Persona");
		mav.addObject("command", new Person());
		return mav;
	}
	@RequestMapping(value = "/Persona", method = RequestMethod.POST)
	public String addArtwork(@ModelAttribute("persona") Person person, BindingResult result, Model map) {
		// ***DB insert Artwork*************************************
				Connection c = null;
				PreparedStatement stmt = null;
				Statement stmtArt = null;
				try {
					Class.forName("org.postgresql.Driver");
					c = DriverManager.getConnection("jdbc:postgresql://localhost:5432/asarurdb", "tesco", "beto1234");
					c.setAutoCommit(false);
					System.out.println("Opened database successfully");
					String sql = "INSERT INTO \"Persona\"(\"documentoNumero\", \"isEstudiante\", \"isArtista\", telefono, apellido, "
							+ "nombre, \"documentoTipo\", \"direccionCalle\", \"direccionNumero\") "
							+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
					stmt = c.prepareStatement(sql);
					stmt.setLong(1, person.getDocumentoNumero());//documentoNumero
					stmt.setBoolean(2, Boolean.parseBoolean(person.getIsEstudiante()));
					stmt.setBoolean(3, Boolean.parseBoolean(person.getIsArtista()));
					stmt.setString(4, person.getTelefono());//telefono
					stmt.setString(5, person.getApellido());//apellido
					stmt.setString(6, person.getNombre());//nombre
					stmt.setString(7, person.getDocumentoTipo());//documentoTipo
					stmt.setString(8, person.getDireccionCalle());//direccionCalle
					stmt.setInt(9, person.getDireccionNumero());//direccionNumero
					stmt.execute();
					c.commit();
					stmt.close();
					c.close();// cierra la conexion a la bd
				} catch (Exception e) {
					System.err.println(e.getClass().getName() + ": " + e.getMessage());
					return "redirect:/Error";
				}
				System.out.println("Records created successfully");
				// ***End DB insert Artwork**********************************
		return "redirect:/Persona";
	}
}
