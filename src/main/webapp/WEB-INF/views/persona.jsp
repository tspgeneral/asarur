<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<h1>
	<spring:message code="persona.title" />
</h1>
<form:form action="Persona" method="POST">
	<table>
		<tr>
			<td><form:label path="documentoNumero">
					<spring:message code="persona.label.documentoNumero" />:
				</form:label></td>
			<td><form:input path="documentoNumero" value=" " /></td>
		</tr>
		<tr>
			<td><form:label path="isEstudiante">
					<spring:message code="persona.select.isEstudiante" />:
				</form:label></td>
			<td><form:select path="isEstudiante" id="isEstudiante">
					<form:option value="true">yes</form:option>
					<form:option value="false">no</form:option>
				</form:select></td>
		</tr>
		<tr>
			<td><form:label path="isArtista">
					<spring:message code="persona.select.isArtista" />:
				</form:label></td>
			<td><form:select path="isArtista" id="isArtista">
					<form:option value="true">yes</form:option>
					<form:option value="false">no</form:option>
				</form:select></td>
		</tr>
		<tr>
			<td><form:label path="telefono">
					<spring:message code="persona.label.telefono" />:
				</form:label></td>
			<td><form:input path="telefono" /></td>
		</tr>
		<tr>
			<td><form:label path="apellido">
					<spring:message code="persona.label.apellido" />:
				</form:label></td>
			<td><form:input path="telefono" /></td>
		</tr>
		<tr>
			<td><form:label path="nombre">
					<spring:message code="persona.label.nombre" />:
				</form:label></td>
			<td><form:input path="nombre" /></td>
		</tr>
		<tr>
			<td><form:label path="documentoTipo">
					<spring:message code="persona.select.documentoTipo" />:
				</form:label></td>
			<td><form:select path="documentoTipo" id="documentoTipo">
					<form:option value="DNI">dni</form:option>
					<form:option value="CUIL">cuil</form:option>
					<form:option value="CUIT">cuit</form:option>
				</form:select></td>
		</tr>
		<tr>
			<td><form:label path="direccionCalle">
					<spring:message code="persona.label.direccionCalle" />:
				</form:label></td>
			<td><form:input path="direccionCalle" /></td>
		</tr>
		<tr>
			<td><form:label path="direccionNumero">
					<spring:message code="persona.label.direccionNumero" />:
				</form:label></td>
			<td><form:input path="direccionNumero" value=" " /></td>
		</tr>
		<tr>
			<td colspan="2" align="right"><input id="addPerson"
				type="submit" class="btn btn-success"
				value="<spring:message code="persona.button.addPerson"/>" /></td>
		</tr>
	</table>
</form:form>