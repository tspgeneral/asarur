<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ page session="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<h1>
	<spring:message code="catalog.title" />
</h1>

<form action="Catalog" method="POST">

	<input type="submit" value="<spring:message code="catalog.button.show"/>" />
	<%
		if (session.getAttribute("listGal") != null) {
	%>

	<c:forEach var="current" items="${sessionScope.listGal}">
		<div class="row">
			<div class="col-sm-3 col-md-6" style="background-color: lavender;">
				<img src="${current.urlImage}" style="width: 450px; height: 300px;" />
			</div>
			<div class="col-sm-9 col-md-6"
				style="background-color: lavenderblush;">
				<p>${current.title}</p>
				<p>${current.address}</p>
				<p>${current.number}</p>
				<p>
					<spring:message code="catalog.inputDni" />
				</p>
				<input type="number" onChange="document.links.Votar${current.cod}.href='Catalog/voto/${current.cod}/'+this.value;"> 
				<a name="Votar${current.cod}" href=""><spring:message code="catalog.vote"/></a>
			</div>
		</div>

	</c:forEach>

	<%
		session.removeAttribute("listGal");
		}
	%>

</form>