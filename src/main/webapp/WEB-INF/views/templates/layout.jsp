<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<!-- 	AIzaSyBUfn3ZhgqD5LYyHOeC7xFUQF6yKm5eSBI -->

<script
	src="http://maps.googleapis.com/maps/api/js?key=AIzaSyBUfn3ZhgqD5LYyHOeC7xFUQF6yKm5eSBI"></script>
<!-- <script src="http://maps.googleapis.com/maps/api/js"></script> -->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script
	src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
	
<script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
	
<style>
  .carousel-inner > .item > img,
  .carousel-inner > .item > a > img {
      width: 70%;
      margin: auto;
  }
</style>
<link rel="stylesheet" href="<spring:theme code="css"/>" type="text/css" />
<title><tiles:insertAttribute name="title" ignore="true" /></title>
<script>
	var map;
	var myCenter = new google.maps.LatLng(-27.4613129, -58.9827234);

	function initialize() {
		var mapProp = {
			center : myCenter,
			zoom : 14,
			mapTypeId : google.maps.MapTypeId.ROADMAP
		};

		map = new google.maps.Map(document.getElementById("googleMap"), mapProp);

		google.maps.event.addListener(map, 'click', function(event) {
			placeMarker(event.latLng);
		});
	}

	function placeMarker(location) {
		
		var marker = new google.maps.Marker({
			position : location,
			map : map,
		});
		var inputC = document.getElementById('coordinate');
		inputC.value = location.lat()+ ',' + location.lng();
		var infowindow = new google.maps.InfoWindow({
			content : 'Latitude: ' + location.lat() + '<br>Longitude: '
					+ location.lng()
		});
		infowindow.open(map, marker);
		
	}

	google.maps.event.addDomListener(window, 'load', initialize);
</script>
</head>
<body>
	<div class="container">
		<div class="masthead">
			<tiles:insertAttribute name="header" />
			<tiles:insertAttribute name="menu" />
		</div>
		<!-- /masthead -->
		<tiles:insertAttribute name="body" />
		<br> <br>
		<tiles:insertAttribute name="footer" />
	</div>
	<!-- /container -->
</body>
</html>