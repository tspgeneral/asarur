<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<h3 class="muted">
	<spring:message code="header.title" />
</h3>
<a class="up-arrow" href="#myPage" data-toggle="tooltip" title="TO TOP">
	<span style="float: left"> <a href="?theme=default">def</a>
		&nbsp;|&nbsp; <a href="?theme=black">blk</a> &nbsp;|&nbsp; <a
		href="?theme=blue">blu</a>
	</span> 
	<span style="float: right"> <a href="?lang=en">en</a>
		&nbsp;|&nbsp; <a href="?lang=es">es</a>
	</span>
</a>