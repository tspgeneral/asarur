<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="/asarur">ASARUR</a>
    </div>
    <div>
      <ul class="nav navbar-nav">
        <li><a href="/asarur"><spring:message code="menu.home"/></a></li>
        <li><a href="Artwork"><spring:message code="menu.artwork"/></a></li>
        <li><a href="Catalog"><spring:message code="menu.catalog"/></a></li>
        <li><a href="Ranking">Ranking</a></li>
        <li><a href="Persona"><spring:message code="menu.register"/></a></li>
      </ul>
    </div>
  </div>
</nav>

