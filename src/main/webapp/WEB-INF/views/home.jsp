<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<h1>
	<spring:message code="home.title"/> 
</h1>

<P><spring:message code="home.message"/>&nbsp;${serverTime}. </P>

<div class="container">
  <br>
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
      <li data-target="#myCarousel" data-slide-to="3"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">

      <div class="item active">
        <img src="/asarur/resources/img/HomeEscul.jpg" alt="Esculturas" width="460" height="345">
        <div class="carousel-caption">
          <h3><spring:message code="home.sculpture"/></h3>
          <p><spring:message code="home.sculptureTi"/></p>
        </div>
      </div>

      <div class="item">
        <img src="/asarur/resources/img/HomeMural.jpg" alt="Murales" width="460" height="345">
        <div class="carousel-caption">
          <h3><spring:message code="home.murals"/></h3>
          <p><spring:message code="home.muralsTi"/></p>
        </div>
      </div>
    
      <div class="item">
        <img src="/asarur/resources/img/HomePintu.jpg" alt="Pinturas" width="460" height="345">
        <div class="carousel-caption">
          <h3><spring:message code="home.paintings"/></h3>
          <p><spring:message code="home.paintingsTi"/></p>
        </div>
      </div>

      <div class="item">
        <img src="/asarur/resources/img/HomeCuadro.jpg" alt="Cuadros" width="460" height="345">
        <div class="carousel-caption">
          <h3><spring:message code="home.pictures"/></h3>
          <p><spring:message code="home.picturesTi"/></p>
        </div>
      </div>
  
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</div>
