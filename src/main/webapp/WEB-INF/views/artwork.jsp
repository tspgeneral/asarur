<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<h1>
	<spring:message code="artwork.title" />
</h1>
<form:form action="Artwork" method="POST" enctype="multipart/form-data">
	<table>
		<tr>
			<td><form:label path="title">
					<spring:message code="artwork.label.title" />:
				</form:label></td>
			<td><form:input path="title" /></td>
		</tr>
		<tr>
			<td><form:label path="esPrivate">
					<spring:message code="artwork.select.esPrivate" />:
				</form:label></td>
			<td><form:select path="esPrivate" id="esPrivate">
					<form:option value="true">private</form:option>
					<form:option value="false">public</form:option>
				</form:select></td>
		</tr>
		<tr>
			<td><form:label path="state">
					<spring:message code="artwork.label.state" />:
				</form:label></td>
			<td><form:input path="state" /></td>
		</tr>
		<tr>
			<td><form:label path="observation">
					<spring:message code="artwork.label.observation" />:
				</form:label></td>
			<td><form:input path="observation" /></td>
		</tr>
		<tr>
			<td><form:label path="dateImp">
					<spring:message code="artwork.label.dateImp" />:
				</form:label></td>
			<td><form:input path="dateImp" /></td>
		</tr>
		<tr>
			<td><form:label path="address">
					<spring:message code="artwork.label.address" />:
				</form:label></td>
			<td><form:input path="address" /></td>
		</tr>
		<tr>
			<td><form:label path="number">
					<spring:message code="artwork.label.number" />:
				</form:label></td>
			<td><form:input path="number" /></td>
		</tr>
		<tr>
			<td>-------------------------------</td>
		</tr>
		<tr>
			<td><form:label path="dniPersonRev">
					<spring:message code="artwork.label.dniPersonRev" />:
				</form:label></td>
			<td><form:input path="dniPersonRev" /></td>
		</tr>
		<tr>
			<td><form:label path="dateRev">
					<spring:message code="artwork.label.dateRev" />:
				</form:label></td>
			<td><form:input path="dateRev" /></td>
		</tr>
		<tr>
			<td>-------------------------------</td>
		</tr>
		<tr>
			<td><form:label path="dniAuthor">
					<spring:message code="artwork.label.dniAuthor" />:
				</form:label></td>
			<td><form:select path="dniAuthor" items="${autorMap}"></form:select></td>
		</tr>
		<tr>
			<td><form:label path="creationDate">
					<spring:message code="artwork.label.creationDate" />:
				</form:label></td>
			<td><form:input path="creationDate" /></td>
		</tr>

		<tr>
			<td><form:label path="nomMaterial">
					<spring:message code="artwork.label.nomMaterial" />:
				</form:label></td>
			<td><form:checkboxes path="nomMaterial" items="${materialMap}"></form:checkboxes></td>
		</tr>
		<tr>
			<td><form:label path="nomCategory">
					<spring:message code="artwork.label.nomCategory" />:
				</form:label></td>
			<td><form:select path="nomCategory" items="${categoriaMap}"></form:select></td>
		</tr>
		<tr>
			<td>------------Images--------------</td>
		</tr>
		<tr>
			<td></td>
			<td><input name="files[0]" type="file" /></td>
		</tr>
		<tr>
			<td></td>
			<td><input name="files[1]" type="file" /></td>
		</tr>
		<tr>
			<td></td>
			<td><input name="files[2]" type="file" /></td>
		</tr>
		<tr>
			<td colspan="2" align="right"><div id="googleMap"
					style="width: 500px; height: 380px;"></div></td>
		</tr>
		<tr>
			<td><form:label path="coordinate">
					<spring:message code="artwork.label.coordinate" />:
				</form:label></td>
			<td><input id="coordinate" name="coordinate" type="text"
				value="Coodinate"></td>
		</tr>
		<tr>
			<td colspan="2" align="right"><input id="addArtwork"
				type="submit" class="btn btn-success"
				value="<spring:message code="artwork.button.addArtwork"/>" /></td>
		</tr>
	</table>
</form:form>

