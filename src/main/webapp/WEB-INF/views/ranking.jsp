<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ page session="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<h1>
	<spring:message code="ranking.title" />
</h1>
<table class="table table-hover">
	<thead>
		<tr class="success">
			<td><spring:message code="ranking.label.numVote" /></td>
			<td><spring:message code="ranking.label.title" /></td>
			<td><spring:message code="ranking.label.address" /></td>
			<td><spring:message code="ranking.label.number" /></td>
			<td><spring:message code="ranking.label.creationDate"/></td>
			<td><spring:message code="ranking.label.state"/></td>
			<td><spring:message code="ranking.label.observation"/></td>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${listTop}" var="artw">
			<tr>
				<td>${artw.numVote}</td>
				<td>${artw.title}</td>
				<td>${artw.address}</td>
				<td>${artw.number}</td>
				<td>${artw.creationDate}</td>
				<td>${artw.state}</td>
				<td>${artw.observation}</td>
			</tr>
		</c:forEach>
	</tbody>
</table>